const express = require("express");

const apicache = require("apicache")
var fs = require('fs');

const fetch = require("node-fetch");
require('dotenv').config()
console.log(process.env)

const PORT = process.env.PORT || 3001;
const app = express();
const axios = require("axios");

// Cache
const cache = apicache.middleware

// Cloud storage dependancies
const { Storage } = require('@google-cloud/storage');
const { GoogleAuth } = require('google-auth-library');
const bucketName = 'kritsite-buffer';
const fileNameShows = 'shows.json';
const fileNameFilms = 'films.json';

async function getAuthenticatedStorageClient() {
    const auth = new GoogleAuth({
      keyFilename: 'kritsite-server-key.json', // Path to your downloaded JSON key file
      scopes: 'https://www.googleapis.com/auth/cloud-platform', // Scope for accessing Google Cloud Storage
    });
  
    const authClient = await auth.getClient();
    const storage = new Storage({ auth: authClient });
  
    return storage;
  }


const DEFAULT_URL = process.env.DEFAULT_ENDPOINT;
const DEFAULT_PARAMETER = process.env.DEFAULT_PARAMETER;
const USERNAME = process.env.API_USERNAME;
const PASSWORD = process.env.API_PASSWORD;

const options = {
    method: 'GET',
    withCredentials: true,
    credentials: 'include',
    // mode: 'no-cors',
    headers: {
        'Accept': 'application/json',
        'Authorization': 'Basic ' + Buffer.from(USERNAME + ':' + PASSWORD).toString('base64'),
        'Access-Control-Allow-Origin': '*'
    },
}

async function downloadJsonFile(bucketName, fileName) {
    const storage = await getAuthenticatedStorageClient();
    try {
      const bucket = storage.bucket(bucketName);
      const file = bucket.file(fileName);
  
      // Download the file as a buffer
      const [content] = await file.download();
  
      // Parse the JSON content from the buffer
      const jsonContent = JSON.parse(content.toString());
  
    //   console.log('JSON content:', jsonContent);
      return jsonContent;
    } catch (error) {
      console.error('Error downloading file:', error);
    }
  }

app.use((req, res, next) => {
    const allowedOrigins = ['http://127.0.0.1:3000', 'http://localhost:3000', 'http://127.0.0.1:3001', 'http://localhost:3001'];
    const origin = req.headers.origin;
    if (allowedOrigins.includes(origin)) {
         res.setHeader('Access-Control-Allow-Origin', origin);
    }
    
    return next();
  });

// app.use(cache('2 minutes'));


// app.get("/", (req, res) => {


//     res.json(
//         async function () {
//             let response = await fetch((defaultEndpoint, options));
//             let responseJson = await response.json()
//             let fromServer = responseJson.myString
//             alert(fromServer)
//         }
//     );
// });

app.get('/shows', async function (req, res) {
        try {
            res.setHeader('Content-Type', 'application/json');
            jsonContent = await downloadJsonFile(bucketName, fileNameShows);

            res.send(jsonContent)
        } catch(err) {
            console.log("Oh no!", err)
        };
});

app.get('/filmsToProductionId', async function (req, res) {
  try {
      res.setHeader('Content-Type', 'application/json');
      jsonContent = await downloadJsonFile(bucketName, fileNameFilms);

      res.send(jsonContent)
  } catch(err) {
      console.log("Oh no!", err)
  };
});

app.get('/getshows/:id', function (req, res) {
    axios.get((DEFAULT_URL + 'shows/info.json?production_id='+ req.params.slug + '&' + DEFAULT_PARAMETER), options)
        .then(response => {
            res.send(response.data)
        })
        .catch(err => {
            console.log("Oh no!", err)
        });
});

app.get('/productions/:slug', function (req, res) {
    axios.get((DEFAULT_URL + '/productions/' + req.params.slug + '?' + DEFAULT_PARAMETER), options)
        .then(response => {
            res.send(response.data)
        })
        .catch(err => {
            console.log("Oh no!", err)
        });
});

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
});

function saveShowsToPublicFolder(shows, callback) {
    fs.writeFile('./public/person.json', JSON.stringify(person), callback);
  }
